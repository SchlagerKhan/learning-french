# The most advanced and epic code in the world...

## Requirements

- Node & npm

## Usage

1.  Clone/Fork project
2.  Go to directory
3.  Run `yarn` or `npm`
4.  Run `yarn start` (or npm...)
5.  Wait for build to complete
6.  Look at hint at the bottom of the page
7.  Conquer world
8.  Don't judge me for using create-react-app
