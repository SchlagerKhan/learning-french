export const MIN_DOT_SIZE = 10; // px
export const MAX_DOT_SIZE = 100; // px

export const NR_DOTS = 10;

export const ANIMATION_DURATION = 3; // Sec
