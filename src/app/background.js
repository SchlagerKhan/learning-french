import styled from 'styled-components';

import background from '../background.jpg';

export default styled.image.attrs({
	href: background,
	preserveAspectRatio: 'xMidYMid slice',
})`
	position: absolute;
	top: 0;
	left: 0;

	width: 100%;
	height: 100%;

	clip-path: url(#dots);
`;
