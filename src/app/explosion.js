// prettier-ignore
import {
	times,
	map,
	filter,
	includes,
} from 'lodash';
import { withState } from 'recompose';

import React, { Component } from 'react';
import PropTypes from 'prop-types';

import styled from 'styled-components';

// prettier-ignore
import {
	MIN_DOT_SIZE,
	MAX_DOT_SIZE,
	NR_DOTS,
	ANIMATION_DURATION,
} from '../config';

import Dot from './dot';

const ANIMATION_DURATION_MS = ANIMATION_DURATION * 1000;

const Wrapper = styled.div`
	position: absolute;
	left: 0;
	top: 0;

	width: 100%;
	height: 100%;

	z-index: 100;
`;

class Explosion extends Component {
	static propTypes = {
		dots: PropTypes.arrayOf(
			PropTypes.shape({
				x: PropTypes.number.isRequired,
				y: PropTypes.number.isRequired,
				size: PropTypes.number.isRequired,
				direction: PropTypes.number.isRequired, // Angle
			}),
		).isRequired,

		setDots: PropTypes.func.isRequired,
	};

	static createDotSize = () => MIN_DOT_SIZE + Math.random() * (MAX_DOT_SIZE - MIN_DOT_SIZE);

	static createDotDirection = () => Math.random() * 2 * Math.PI;

	static createDot = origin => ({
		id: Date.now() * Math.random(),
		x: origin.x,
		y: origin.y,
		size: Explosion.createDotSize(),
		direction: Explosion.createDotDirection(),
	});

	static createDots = (nr, origin) => times(nr, () => Explosion.createDot(origin));

	/* METHODS */
	explode = (origin) => {
		const { dots, setDots } = this.props;
		const _dots = Explosion.createDots(NR_DOTS, origin);

		const newDots = [...dots, ..._dots];

		setDots(newDots);

		this.startDotsTimer(_dots);
	};

	startDotsTimer = (_dots) => {
		const self = this;
		const ids = map(_dots, 'id');

		setTimeout(() => {
			const { dots, setDots } = self.props;
			const newDots = filter(dots, dot => !includes(ids, dot.id));

			setDots(newDots);
		}, ANIMATION_DURATION_MS);
	};

	/* EVENT HANDLERS */
	handleClick = (e) => {
		const x = e.pageX;
		const y = e.pageY;

		this.explode({ x, y });
	};

	/* RENDERING */
	render() {
		const { dots } = this.props;

		const _dots = dots.map(dotProps => <Dot key={`dot-${dotProps.id}`} {...dotProps} />);

		// prettier-ignore
		return (
			<Wrapper onClick={this.handleClick}>
				<svg>
					<clipPath id='dots'>
						{_dots}
					</clipPath>
				</svg>
			</Wrapper>
		);
	}
}

const enhance = withState('dots', 'setDots', []);

export default enhance(Explosion);
