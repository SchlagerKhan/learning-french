/* eslint-disable react/destructuring-assignment */

import { withState } from 'recompose';

import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';

import styled from 'styled-components';

import { ANIMATION_DURATION } from '../config';

const DUR = `${ANIMATION_DURATION}s`;
const SCREEN_OFFSET = 1000;

const Ellipse = styled.ellipse.attrs({
	rx: props => props.size,
	ry: props => props.size,
})`
	cx: ${props => props.x};
	cy: ${props => props.y};

	transition: cx ${DUR}, cy ${DUR};
`;

class Dot extends PureComponent {
	static propTypes = {
		x: PropTypes.number.isRequired,
		y: PropTypes.number.isRequired,
		size: PropTypes.number.isRequired,
		direction: PropTypes.number.isRequired,

		animating: PropTypes.bool.isRequired,
		setAnimating: PropTypes.func.isRequired,
	};

	componentDidMount() {
		const { setAnimating } = this.props;
		const start = () => setAnimating(true);

		setTimeout(start, 0);
	}

	render() {
		// prettier-ignore
		const {
			x,
			y,
			size,
			direction,
			animating,
		} = this.props;

		const toX = x + Math.sin(direction) * SCREEN_OFFSET;
		const toY = y + Math.cos(direction) * SCREEN_OFFSET;

		const style = animating ? { cx: toX, cy: toY } : {};

		// prettier-ignore
		return (
			<Ellipse
				size={size}
				x={x}
				y={y}
				style={style}
			/>
		);
	}
}

const enhance = withState('animating', 'setAnimating', false);

export default enhance(Dot);
