import React, { Fragment } from 'react';

import styled from 'styled-components';

const Text = styled.text`
	width: 100%;

	font-size: 148px;
	text-anchor: middle;

	fill: white;

	clip-path: url(#dots);
`;

// prettier-ignore
export default () => (
	<Fragment>
		<Text id='title' x='50%' y='50%'>ELEMENTUS</Text>
	</Fragment>
);
