import React from 'react';

import styled from 'styled-components';

import Background from './background';
import Title from './title';
import Explosion from './explosion';

const Wrapper = styled.div`
	width: 100%;
	height: 100vh;
`;

const Svg = styled.svg`
	width: 100%;
	height: 100%;

	display: flex;
	align-items: center;
	justify-content: center;

	fill: black;
`;

const Hint = styled.p`
	position: absolute;
	left: 0;
	bottom: 10px;

	width: 100%;

	font-size: 16px;
	text-align: center;
`;

export default () => (
	<Wrapper>
		<Svg>
			<Background />
			<Title />
		</Svg>
		<Explosion />
		<Hint>Click anywhere</Hint>
	</Wrapper>
);
